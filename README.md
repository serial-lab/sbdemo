# SerialBox Demo Project
This project illustrates some of the examples in the SerialBox documentation and also provides a simple Django project for getting started.

See the [SerialBox Documentation](https://serial-lab.gitlab.io/serialbox/) for 
general usage.  If you're looking to use this project as guidance for writing a
custom *FlavorPack*, check out the [Custom FlavorPacks Section](https://serial-lab.gitlab.io/serialbox/flavorpacks/index.html).
