import sys
from django.contrib.auth.models import User
from django.test import TestCase
from django.core.urlresolvers import reverse

from rest_framework import status

from serialbox.utils import get_region_by_machine_name


import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('word_unit_tests')


class WordRegionTests(TestCase):

    def setUp(self):
        user = User.objects.create(username='testuser',
                                   password='unittest',
                                   email='testuser@seriallab.local')
        logger.info(self.client.force_login(user))
        region_data = {
            "pool": "wrtpool1",
            "readable_name": "Unit Test Word Region",
            "machine_name": "utwr",
            "active": True,
            "order": 1,
            "last_word_line": 1
        }
        self.logging_setup()
        self.create_pool()
        self.create_region(region_data)

    def logging_setup(self):
        import logging
        import sys

        root = logging.getLogger()
        root.setLevel(logging.INFO)

        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        root.addHandler(ch)

    def create_pool(self, data=None, assert_status=status.HTTP_201_CREATED):
        '''
        Ensure we can create a new pool instance.
        '''
        data = data or {
            "readable_name": "Word Region Test Pool",
            "machine_name": "wrtpool1",
            "active": "true",
            "request_threshold": 100
        }
        url = reverse('pool-create')
        response = self.client.post(url, data, format='json')
        logger.info(response.content)
        self.assertEqual(response.status_code, assert_status)

    def create_region(self, data, assert_status=status.HTTP_201_CREATED):
        '''
        Ensure we can create a test region for the created pool instance
        '''
        url = reverse('word-region-create')
        response = self.client.post(url, data, format='json')
        logger.info(response.content)
        self.assertEqual(response.status_code, assert_status)
        return response

    def test_get_region_by_machine_name(self):
        machine_name = 'utwr'
        region = get_region_by_machine_name(machine_name)
        self.assertTrue(
            region is not None,
            'Could not look up region %s using '
            'the get_region_by_machine_name utility.' % machine_name)
        logger.debug('word region was retrieved...')


    def test_allocate_numbers(self):
        '''
        Ensure we can get numbers from the pool
        '''
        url = reverse('allocate-numbers', args=['wrtpool1', '100'])
        response = self.client.get(url, format='json')
        logger.info(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
