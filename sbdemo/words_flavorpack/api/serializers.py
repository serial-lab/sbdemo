'''
    Copyright 2015 SerialLab, LLC

    This file is part of sbdemo.

    sbdemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    sbdemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with sbdemo.  If not, see <http://www.gnu.org/licenses/>.
'''

from rest_framework import serializers as rf_serializers
from serialbox.api.serializers import RegionSerializer
from sbdemo.words_flavorpack.models import WordRegion


from sbdemo.words_flavorpack import models

##
# These fields will be added to the pool serializer automatically since
# they are specified in the pool_slug_fields and the pool_hyperlinked_fields
# values in the words_flavorpack.apps.WordsFlavorpackConfig module.
##
wordregion_set = rf_serializers.SlugRelatedField(
    many=True,
    queryset=models.WordRegion.objects.all(),
    slug_field='machine_name',
    required=False
)

wordregion_hyperlink_set = rf_serializers.HyperlinkedRelatedField(
    many=True,
    read_only=True,
    view_name='word-region',
    lookup_field='machine_name',
)


class WordRegionSerializer(RegionSerializer):
    '''
    Specifies the model...excludes the id...
    '''
    class Meta(object):
        model = WordRegion
        exclude = ('id',)


class HyperlinkedWordRegionSerializer(WordRegionSerializer):
    '''
    Addst the hyperlinked field...
    '''
    pool = rf_serializers.HyperlinkedRelatedField(view_name='pool-detail',
                                                  read_only=True,
                                                  lookup_field='machine_name')
