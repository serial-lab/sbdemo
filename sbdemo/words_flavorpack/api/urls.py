'''
    Copyright 2015 SerialLab, LLC

    This file is part of sbdemo.

    sbdemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    sbdemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with sbdemo.  If not, see <http://www.gnu.org/licenses/>.
'''

from django.conf.urls import url

from sbdemo.words_flavorpack.api import viewsets

urlpatterns = [
    url(r'^word-regions/$',
        viewsets.word_region_list,
        name='word-region-list'),
    url(r'^word-region-create/$',
        viewsets.word_region_create,
        name='word-region-create'),
    url(r'^word-region-detail/(?P<machine_name>[0-9a-zA-Z]{1,100})/$',
        viewsets.word_region_detail,
        name='word-region-detail'),
    url(r'^word-region-modify/(?P<machine_name>[0-9a-zA-Z]{1,100})/$',
        viewsets.word_region_modify,
        name='word-region-modify'),
    url(r'^word-region-form/(?P<machine_name>[0-9a-zA-Z]{1,100})/$',
        viewsets.word_region_form,
        name='word-region-form'),
]
