'''
    Copyright 2015 SerialLab, LLC

    This file is part of sbdemo.

    sbdemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    sbdemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with sbdemo.  If not, see <http://www.gnu.org/licenses/>.
'''
from serialbox.api.viewsets import FormMixin
from serialbox import viewsets

from sbdemo.words_flavorpack.models import WordRegion
from sbdemo.words_flavorpack.api import serializers


class WordRegionViewSet(viewsets.SerialBoxModelViewSet, FormMixin):
    queryset = WordRegion.objects.all()
    lookup_field = 'machine_name'

    def get_serializer_class(self):
        '''
        Return a different serializer depending on the client request.
        '''
        ret = serializers.WordRegionSerializer
        try:
            if self.request.query_params.get('related') == 'true':
                ret = serializers.HyperlinkedWordRegionSerializer
        except AttributeError:
            pass
        return ret


word_region_list = WordRegionViewSet.as_view({
    'get': 'list'
})
word_region_create = WordRegionViewSet.as_view({
    'post': 'create'
})
word_region_detail = WordRegionViewSet.as_view({
    'get': 'retrieve'
})
word_region_modify = WordRegionViewSet.as_view({
    'put': 'partial_update',
    'delete': 'destroy'
})
word_region_form = WordRegionViewSet.as_view({
    'get': 'form'
})