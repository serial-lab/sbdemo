from django.db import models
from django.utils.translation import gettext_lazy as _

import serialbox.models as sb_models


class WordRegion(sb_models.Region):
    '''
    A word region generates lists of words from a language dictionary.
    '''
    last_word_line = models.IntegerField(
        verbose_name=_('Last Word Line'),
        help_text=_('The line number of the last word issued '
                    'maintains the state of the word based region.'),
        default=1)
