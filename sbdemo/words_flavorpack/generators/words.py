'''
    Copyright 2015 SerialLab, LLC

    This file is part of sbdemo.

    sbdemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    sbdemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with sbdemo.  If not, see <http://www.gnu.org/licenses/>.
'''

import linecache
from serialbox.generators.common import Generator


class WordGenerator(Generator):
    '''
    This generator will create lists of words from the
    '''

    def generate(self, request, response, region, size):
        # change this to use a different file
        filename = '/usr/share/dict/words'
        # get that last word line from the region
        first = region.last_word_line
        # grab the n number of lines after that word based
        # on the size of the request
        lines = []
        max_line = first + size
        for lineno in range(first, max_line):
            lines.append(linecache.getline(filename, lineno).strip())
        # just for fun set the encoding to us-english using the ISO code
        response.encoding = 'en-US'
        # set the state of the region to the last line
        region.last_word_line = max_line
        # always call set_number_list in generators by passing
        # in the list of (whatever) you've generated
        return self.set_number_list(response, lines)
