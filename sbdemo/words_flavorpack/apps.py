from serialbox.flavor_packs import FlavorPackApp


class WordsFlavorpackConfig(FlavorPackApp):
    # The flavorpack app is a django AppConfig instance
    # with a meta-class that does some auto-registration for flavorpacks...
    # for more on AppConfigs see the Django documentation.
    name = 'sbdemo.words_flavorpack'

    @property
    def pool_slug_fields(self):
        return {
            'wordregion_set':
            'sbdemo.words_flavorpack.api.serializers.wordregion_set'
        }

    # Each flavorpack must supply hyperlink fields for it's serializer fields
    @property
    def pool_hyperlink_fields(self):
        return {
            'wordregion_set':
            'sbdemo.words_flavorpack.api.serializers.wordregion_hyperlink_set'
        }
    # Each flavorpack must supply number generators for it's regions

    @property
    def generators(self):
        # here we define the model and generator that
        # is responsible for creating words for the API and maintaining
        # the appropriate state in the database
        return {
            'sbdemo.words_flavorpack.models.WordRegion':
            'sbdemo.words_flavorpack.generators.words.WordGenerator'
        }

    @property
    def api_urls(self):
        # this is the name of the URL config that returns word regions,
        # returning it makes it visible on the self-documenting
        # django rest framework HTML interface at the Root Api page...
        return ['word-region-list']
